package hangman;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import javax.swing.JPanel;

/**
 *
 * @author philipjnr.habib
 */
public class HangManDrawing extends JPanel
{
    private static final long serialVersionUID = 1L;
    private Graphics2D g2d;
    private String guesses;
    private int headx = -100, heady = -100, headdi = 25;
    private int bodyx1 = -100, bodyy1 = -100,bodyx2 = -100, bodyy2 = -100;
    private int larmx1 = -100, larmy1 = -100,larmx2 = -100, larmy2 = -100;
    private int rarmx1 = -100, rarmy1 =  -100,rarmx2 = -100, rarmy2 = -100;
    private int llegx1 = -100, llegy1 = -100,llegx2 = -100, llegy2 = -100;
    private int rlegx1 = -100, rlegy1 = -100,rlegx2 = -100, rlegy2 = -100;
    int count = 0;

    @Override
    public void paint(Graphics g)
    {
        super.paint(g);
        g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        //HANGER
        g2d.drawLine(113, 60, 113, 40);
        g2d.drawLine(113, 40, 150, 40);
        g2d.drawLine(150, 40, 150, 175);
        g2d.drawLine(100, 175, 200, 175);

        //PERSON
        //Head
        g2d.drawOval(headx, heady, headdi, headdi);
        //Body
        g2d.drawLine(bodyx1,bodyy1, bodyx2, bodyy2);
        //Left Leg
        g2d.drawLine(llegx1, llegy1, llegx2, llegy2);
        //Right Leg
        g2d.drawLine(rlegx1, rlegy1, rlegx2, rlegy2);
        //Left Arm
        g2d.drawLine(larmx1, larmy1, larmx2, larmy2);
        //Right Arm
        g2d.drawLine(rarmx1, rarmy1, rarmx2, rarmy2);
    }

    public boolean drawNext()
    {
        count++;
        switch (count)
        {
            case 1:
                headx = 100;
                heady = 60;
                repaint();
                return false;
            case 2:
                bodyx1 = 113;
                bodyy1 = 85;
                bodyx2 = 113;
                bodyy2 = 125;
                repaint();
                return false;
            case 3:
                larmx1 = 113;
                larmy1 = 90;
                larmx2 = 100;
                larmy2 = 120;
                repaint();
                return false;
            case 4:
                rarmx1 = 113;
                rarmy1 = 90;
                rarmx2 = 126;
                rarmy2 = 120;
                repaint();
                return false;
            case 5:
                llegx1 = 113;
                llegy1 = 125;
                llegx2 = 100;
                llegy2 = 160;
                repaint();
                return false;
            case 6:
                rlegx1 = 113;
                rlegy1 = 125;
                rlegx2 = 126;
                rlegy2 = 160;
                repaint();
                return false;
            default:
                return true;
        }
    }

}
