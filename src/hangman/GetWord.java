package hangman;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author philipjnr.habib
 */
public class GetWord extends JFrame
{

    private String[] movies = new String[]
    {
        "The Shawshank Redemption",
        "The Godfather",
        "The Godfather",
        "The Dark Knight",
        "Pulp Fiction",
        "Schindler's List",
        "The Good, the Bad and the Ugly",
        "The Lord of the Rings: The Return of the King",
        "Fight Club",
        "The Lord of the Rings",
        "Star Wars",
        "Forrest Gump",
        "Inception"
    };
    private String[] artists = new String[]
    {
        "Taylor Swift",
        "Meghan Trainor",
        "Nicki Minaj",
        "Maroon 5",
        "Sam Smith",
        "Ed Sheeran",
        "The Weeknd",
        "Mark Ronson",
        "Nick Jonas",
        "Rihanna",
        "Big Sean",
        "Sia",
        "Ariana Grande",
        "Pitbull",
        "Ellie Goulding",
        "Tove Lo",
        "Flo Rida",
        "Fall Out Boy",
        "Fetty Wap",
        "Walk the Moon"
    };
    private String[] cities = new String[]
    {
        "Toronto",
        "Montreal",
        "Calgary",
        "Ottawa",
        "Edmonton",
        "Mississauga",
        "Winnipeg",
        "Vancouver",
        "Brampton",
        "Hamilton",
        "Quebec City",
        "Surrey",
        "Laval",
        "Halifax",
        "London",
    };

    public GetWord()
    {
        super("Choose");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200, 100);
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);
    }

    private void init()
    {
        JPanel mainPanel = new JPanel();
        final JComboBox<String> categories = new JComboBox<>(getCategories());

        mainPanel.add(categories);
        JButton submit = new JButton("Submit");
        submit.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                String wordToGuess = randomWord(categories.getSelectedItem().toString()).toUpperCase();
                if (categories.getSelectedItem().toString().equals("Categories"))
                {
                    JOptionPane.showMessageDialog(null, "Please enter a category to play!", "Nerd", JOptionPane.ERROR_MESSAGE);
                } else
                {
//                    System.out.println(wordToGuess);
                    new HangManGame(wordToGuess, categories.getSelectedItem().toString());
                }
            }
        });
        mainPanel.add(submit);
        setContentPane(mainPanel);
        
    }

    private String randomWord(String category)
    {
        if (category.equals("Movies"))
        {
            int len = movies.length;
            int ran = (int) (Math.random() * len);
            return movies[ran];
        } else if (category.equals("Song Artists"))
        {
            int len = artists.length;
            int ran = (int) (Math.random() * len);
            return artists[ran];
        } else if (category.equals("Cities"))
        {
            int len = cities.length;
            int ran = (int) (Math.random() * len);
            return cities[ran];
        }
        return "Philip";
    }

    private String[] getCategories()
    {
        return new String[]
        {
            "Categories", "Movies", "Song Artists", "Cities", "Awesome People"
        };
    }

}
