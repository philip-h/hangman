package hangman;

import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

/**
 *
 * @author philipjnr.habib
 */
public class HangManGame extends JFrame implements ActionListener
{
    private static final long serialVersionUID = 1L;
    JLabel[] lettersToGuess;
    private String theWord, theCategory;
    private HangManDrawing hmd;

    public HangManGame(String theWord, String theCategory) throws HeadlessException
    {
        super("Philip's Awesome Hangman Game!");
        this.theWord = theWord;
        this.theCategory = theCategory;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(410, 400);
        setLocationRelativeTo(null);
        setResizable(false);
        setVisible(true);
    }

    private void init()
    {
        JPanel mainPane = new JPanel(new BorderLayout());
        hmd = new HangManDrawing();
        mainPane.add(hmd, BorderLayout.CENTER);
        mainPane.add(getGuessPanel(), BorderLayout.NORTH);
        mainPane.add(getKeyboardPanel(), BorderLayout.SOUTH);
        setContentPane(mainPane);

        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("Click Me!");
        JMenuItem newCategory = new JMenuItem("New Category");
        newCategory.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
        newCategory.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                dispose();
                new GetWord();
            }
        });
        menu.add(newCategory);

        JMenuItem guessWord = new JMenuItem("Guess Word");
        guessWord.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, ActionEvent.CTRL_MASK));
        guessWord.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                String guess = JOptionPane.showInputDialog("Make a guess");
                if (guess.toUpperCase().equals(theWord))
                {
                    JOptionPane.showMessageDialog(null, "Congratz!! You Win!", "WINNER", JOptionPane.PLAIN_MESSAGE);
                    dispose();
                    new GetWord();
                } else
                {
                    if (hmd.drawNext())
                    {
                        JOptionPane.showMessageDialog(null, "You Lose! The word was: " + theWord, "LOSER", JOptionPane.PLAIN_MESSAGE);
                        dispose();
                        new GetWord();
                    }
                }
            }
        });
        menu.add(guessWord);
        menuBar.add(menu);
        JMenuItem category = new JMenuItem(theCategory);
        menuBar.add(category);
        setJMenuBar(menuBar);

    }

    private JPanel getGuessPanel()
    {
        JPanel guessPanel = new JPanel();
        int len = theWord.length();
        lettersToGuess = new JLabel[len];
        for (int i = 0; i < len; i++)
        {
            if (theWord.charAt(i) == ' ')
            {
                lettersToGuess[i] = new JLabel(" ");
            } else if (Character.isLetter(theWord.charAt(i)))
            {
                lettersToGuess[i] = new JLabel("_ ");
            } else
            {
                lettersToGuess[i] = new JLabel(theWord.charAt(i) + "");
            }
            guessPanel.add(lettersToGuess[i]);
        }
        guessPanel.setBorder(BorderFactory.createEtchedBorder());
        return guessPanel;
    }

    private JPanel getKeyboardPanel()
    {
        JPanel keyboardPanel = new JPanel(new GridLayout(4, 7, 10, 5));
        keyboardPanel.setBorder(BorderFactory.createEtchedBorder());
        JButton[] keyboard = new JButton[26];
        for (int i = 0; i < keyboard.length; i++)
        {
            char letter = (char) ((char) i + 'A');
            keyboard[i] = new JButton(letter + "");
            keyboard[i].addActionListener(this);
            keyboardPanel.add(keyboard[i]);
        }
        return keyboardPanel;
    }

    private String getRandomWord()
    {
        return "PHILIP";
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {

        JButton letterPressed = (JButton) e.getSource();
        String letter = letterPressed.getText();
        if (theWord.contains(letter))
        {
            String word = theWord;
            while (word.contains(letter))
            {
                int index = word.indexOf(letter);
                lettersToGuess[index].setText(letter);
                word = word.replaceFirst(letter, "`");
            }
            boolean done = true;
            for (int i = 0; i < lettersToGuess.length; i++)
            {
                if (lettersToGuess[i].getText().equals("_ "))
                {
                    done = false;
                }
            }
            if (done)
            {
                JOptionPane.showMessageDialog(null, "Congratz!! You Win!", "WINNER", JOptionPane.PLAIN_MESSAGE);
                this.dispose();
                new GetWord();
            }
        } else
        {
            if (hmd.drawNext())
            {
                JOptionPane.showMessageDialog(null, "You Lose! The word was: " + theWord, "LOSER", JOptionPane.PLAIN_MESSAGE);
                this.dispose();
                new GetWord();
            }

        }
        letterPressed.setEnabled(false);

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        new GetWord();
    }

}
